# Importações do Django
from rest_framework import serializers

# Importações do Sistema
from .models import Usuario


# Usuario Serializador
class UsuarioSerializador(serializers.ModelSerializer):

    class Meta:

        # Modelo que será serializado
        model = Usuario

        # Campos que serão serializados
        fields = ["id", "nome_usuario", "celular_usuario", "cpf_usuario", "email_usuario", "password_usuario", "tipo_usuario"]
        


# Serialização para o recebimento de email
class RecuperarSenhaEmailSerializador(serializers.Serializer):

    # Criação do campo email
    email = serializers.EmailField()
    nome = serializers.CharField(max_length=255)
    assunto = serializers.CharField(max_length=255)
    mensagem = serializers.CharField(max_length=255)

    class Meta:

        # Campo que será serializado
        fields = ["email", "nome", "assunto", "mensagem"]
