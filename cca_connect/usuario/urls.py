# Importações do Django
from django.urls import path

# Importações do Sistema
from .views import *


# Usuário URLS
urlpatterns = [

    path("", UsuarioListar.as_view(), name="usuario-listar"),
    path("<int:pk>/", UsuarioDetalhar.as_view(), name="usuario-detalhar"),
    path("cadastrar/", UsuarioCadastrar.as_view(), name="usuario-cadastrar"),
    path("atualizar/<int:pk>/", UsuarioAtualizar.as_view(), name="usuario-atualizar"),
    path("deletar/<int:pk>/", UsuarioDeletar.as_view(), name="usuario-deletar"),
    
    path("chamado/", RecuperarSenhaEmailEnviar.as_view())

]
