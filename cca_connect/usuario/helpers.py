# Importações do Django
import smtplib
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.template.loader import render_to_string

# Importações do Sistema
from cca_connect.settings import EMAIL_HOST, EMAIL_PASSWORD, EMAIL_PORT, EMAIL_USER


# Classe para enviar o email de recuperação de senha
class EmailEnviar:

    # Método que recebe como parâmetro o email do Usuário em que o sistema irá mandar a mensagem, o id codificado desse Usuário
    # e o Token de validação desse Usuário e o nome desse Usuário
    def mensagem_email_enviar(email, nome_usuario, assunto, mensagem):

        # Dados do email
        assunto_email = "Mensagem Enviada para o Sistema - CCA CONNECT"
        email_que_vai_enviar = EMAIL_USER
        email_que_vai_receber = EMAIL_USER

        # Carregando imagem do logotipo
        imagem = open("assets/images/LOGO CCA CONECT.png", "rb")
        logo_imagem = MIMEImage(imagem.read())
        imagem.close()

        try:

            # Criando o corpo do email a ser enviado
            email = MIMEMultipart("related")
            mensagem_alternativa = MIMEMultipart('alternative')
            email["Subject"] = assunto_email
            email["From"] = email_que_vai_enviar
            email["To"] = email_que_vai_receber
            email.preamble = "This is a multi-part message in MIME format."

            # Configurando a mensagem que irá aparecer para o Usuário no email
            conteudo_html = render_to_string("enviar-mensagem.txt", {
                                             "nome_usuario": nome_usuario, "email": email_que_vai_receber, "mensagem": mensagem, "assunto": assunto})

            # Anexar mensagens no email
            email.attach(mensagem_alternativa)
            texto_mensagem = MIMEText(conteudo_html, "html")
            mensagem_alternativa.attach(texto_mensagem)

            # Anexar imagem do logotipo no email
            logo_imagem.add_header("Content-ID", "<logo>")
            email.attach(logo_imagem)

            # Conectando ao serviço SMTP
            smtp = smtplib.SMTP_SSL(EMAIL_HOST, int(EMAIL_PORT))

            # Efetuando o login na conta do gmail da solução
            smtp.login(EMAIL_USER, EMAIL_PASSWORD)

            # Enviando o email
            smtp.sendmail(email_que_vai_enviar,
                          email_que_vai_receber, email.as_string())

            # Encerrando o serviço SMTP
            smtp.close()

        except:

            raise Exception
        

    # e o Token de validação desse Usuário e o nome desse Usuário
    def cliente(email, nome_usuario):

        # Dados do email
        assunto_email = "Mensagem Enviada para o Sistema - CCA CONNECT"
        email_que_vai_enviar = EMAIL_USER
        email_que_vai_receber = email

        # Carregando imagem do logotipo
        imagem = open("assets/images/LOGO CCA CONECT.png", "rb")
        logo_imagem = MIMEImage(imagem.read())
        imagem.close()

        try:

            # Criando o corpo do email a ser enviado
            email = MIMEMultipart("related")
            mensagem_alternativa = MIMEMultipart('alternative')
            email["Subject"] = assunto_email
            email["From"] = email_que_vai_enviar
            email["To"] = email_que_vai_receber
            email.preamble = "This is a multi-part message in MIME format."

            # Configurando a mensagem que irá aparecer para o Usuário no email
            conteudo_html = render_to_string("cliente.txt", {
                                             "nome_usuario": nome_usuario, "email_cca": email_que_vai_enviar, })

            # Anexar mensagens no email
            email.attach(mensagem_alternativa)
            texto_mensagem = MIMEText(conteudo_html, "html")
            mensagem_alternativa.attach(texto_mensagem)

            # Anexar imagem do logotipo no email
            logo_imagem.add_header("Content-ID", "<logo>")
            email.attach(logo_imagem)

            # Conectando ao serviço SMTP
            smtp = smtplib.SMTP_SSL(EMAIL_HOST, int(EMAIL_PORT))

            # Efetuando o login na conta do gmail da solução
            smtp.login(EMAIL_USER, EMAIL_PASSWORD)

            # Enviando o email
            smtp.sendmail(email_que_vai_enviar,
                          email_que_vai_receber, email.as_string())

            # Encerrando o serviço SMTP
            smtp.close()

        except:

            raise Exception

 