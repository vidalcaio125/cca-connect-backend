# Importações do Django
from django.db import models
from django.utils.translation import gettext_lazy as _


# Modelo do Usuário
class Usuario(models.Model):

    # Tipos de usuarios do sistema
    # 0 - Admin
    # 1 - Cliente

    # Dados do Usuario
    tipo_usuario = models.IntegerField(_("Tipo do Usuário"), default=0)
    nome_usuario = models.CharField (_("Nome do Usuário"), max_length=255)
    celular_usuario = models.CharField(_("Celular do Usuário"), max_length=15)
    cpf_usuario = models.CharField(_("CPF do Usuário"), max_length=14, unique=True)

    # Dados de acesso e permissão
    email_usuario = models.EmailField (_("Email do Usuário"), max_length=255, unique=True)
    password_usuario = models.CharField(_("Senha do Usuário"), max_length=255)

    # Retorno padrão do objeto da classe
    def __str__(self):
        return self.nome_usuario
    
    # Configurando o nome da tabela no banco de dados e o nome do aplicativo
    class Meta:
        db_table = "usuario"
        app_label = "usuario"

    




