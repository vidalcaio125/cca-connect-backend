# Importações do Django
from rest_framework import generics, response, status

# Importações do Sistema
from .serializers import UsuarioSerializador, RecuperarSenhaEmailSerializador
from .models import Usuario
from .helpers import EmailEnviar


# Método GET - Lista de Usuários
class UsuarioListar(generics.ListAPIView):

    # Definindo o queryset do método
    queryset = Usuario.objects.all()

    # Sobrescrevendo a função do método GET
    def get(self, request): 

        # Serializando o usuário
        usuario_serializer = self.serializer_class(self.queryset.all(), many = True)

        # Resposta ao método GET - Enviando todos os usuários do sistema
        return response.Response(data = usuario_serializer.data, status = status.HTTP_200_OK)

    # Definindo a classe do serializador do método
    serializer_class = UsuarioSerializador


# Método GET - Detalhes do Usuário
class UsuarioDetalhar(generics.RetrieveAPIView):

    # Definindo o queryset do método
    queryset = Usuario.objects.all()

    # Sobrescrevendo a função do método GET
    def get(self, request, pk): 

        # Serializando o usuário
        usuario_serializer = self.serializer_class(self.queryset.get(pk = pk), many = False)

        # Resposta ao método GET - Enviando os dados do usuário do sistema
        return response.Response(data = usuario_serializer.data, status = status.HTTP_200_OK)

    # Definindo a classe do serializador do método
    serializer_class = UsuarioSerializador


# Método POST - Cadastrar Usuário
class UsuarioCadastrar(generics.CreateAPIView):

    # Definindo o queryset do método
    queryset = Usuario.objects.all()

    # Sobrescrevendo a função do método POST
    def post(self, request):

        # Serializando os dados da requisição
        usuario_serializer = self.serializer_class(data = request.data)

        # Verificando se o serializador do usuário é válido
        if usuario_serializer.is_valid():

            # Salvando o usuário
            usuario_serializer.save()

            # Resposta ao método POST - Enviando os dados do usuário criado no sistema
            return response.Response(data = usuario_serializer.data, status = status.HTTP_201_CREATED)
    
        else:

            # Resposta ao método POST - Enviando os dados inválidos do usuário no sistema
            return response.Response(data = usuario_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
    
    # Definindo a classe do serializador do método
    serializer_class = UsuarioSerializador


# Método UPDATE - Atualizar o Usuário
class UsuarioAtualizar(generics.RetrieveUpdateAPIView):

    # Definindo o queryset do método
    queryset = Usuario.objects.all()

    # Sobrescrevendo a função do método UPDATE
    def update(self, request, pk):

        # Carregando os dados do usuário que será atualizado
        usuario = self.queryset.get(pk = pk)

        # Serializando os dados da requisição e sobrescrevendo os dados do usuário a ser atualizado
        usuario_serializer = self.serializer_class(usuario, request.data)

        # Verificando se o serializador do usuário é válido
        if usuario_serializer.is_valid():

            # Salvando o usuário
            usuario_serializer.save()

            # Resposta ao método POST - Enviando os dados do usuário atualizado no sistema
            return response.Response(data = usuario_serializer.data, status = status.HTTP_201_CREATED)
    
        else:

            # Resposta ao método POST - Enviando os dados inválidos do usuário no sistema
            return response.Response(data = usuario_serializer.errors, status = status.HTTP_400_BAD_REQUEST)
    
    # Definindo a classe do serializador do método
    serializer_class = UsuarioSerializador


# Método DELETE- Deletar o Usuário
class UsuarioDeletar(generics.RetrieveDestroyAPIView):

    # Definindo o queryset do método
    queryset = Usuario.objects.all()

    # Sobrescrevendo a função do método DELETE
    def delete(self, request, pk):

        # Carregando os dados do usuário que será excluído
        usuario = self.queryset.get(pk = pk)

        # Serializando o usuário
        usuario_serializer = self.serializer_class(usuario, many = False)

        # Deletando o usuário
        usuario.delete()

        # Resposta ao método DELETE - Enviando os dados do uusário deletado no sistema
        return response.Response(data = usuario_serializer.data, status = status.HTTP_204_NO_CONTENT)
    
    # Definindo a classe do serializador do método
    serializer_class = UsuarioSerializador


# Envio do email para a recuperação de senha
class RecuperarSenhaEmailEnviar(generics.GenericAPIView):

    authentication_classes = ()
    permission_classes = ()

    serializer_class = RecuperarSenhaEmailSerializador
    queryset = Usuario.objects.all()

    def post(self, request):

        email = request.data['email']
        nome = request.data['nome']
        assunto = request.data['assunto']
        mensagem = request.data['mensagem']

        try:

            # Chamando a função que enviará o email para o Usuário
            EmailEnviar.mensagem_email_enviar(email, nome, assunto, mensagem)
            
            EmailEnviar.cliente(email, nome)

            return response.Response(data = "Verifique sua caixa de entrada!", status = status.HTTP_200_OK)

        except:

            return response.Response(data = 'Problema no processamento da sua solicitação! Entre em contato com o suporte!', status = status.HTTP_400_BAD_REQUEST)

