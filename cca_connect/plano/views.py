from rest_framework import generics, status, response
from .models import Plano
from .serializers import PlanoSerializador


class PlanoListar(generics.ListAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializador

class PlanoDetalhar (generics.RetrieveAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializador
  
class PlanoCadastrar (generics.CreateAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializador

class PlanoAtualizar (generics.RetrieveUpdateAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializador

class PlanoDeletar (generics.RetrieveDestroyAPIView):
    queryset = Plano.objects.all ()
    serializer_class = PlanoSerializador






