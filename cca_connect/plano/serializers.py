# Importações do Django
from rest_framework import serializers

# Importações do Sistema
from .models import Plano


# Plano Serializador
class PlanoSerializador(serializers.ModelSerializer):

    class Meta:

        # Modelo que será serializado
        model = Plano

        # Campos que serão serializados
        fields = "__all__"
