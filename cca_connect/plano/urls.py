# # Django imports
# from django.urls import path

# # System imports
# from .views import *


# # Usuário URLS
# urlpatterns = [

#     path("", PlanoList.as_view(), name="Plano-list"),
#     path("<int:pk>/", PlanoDetail.as_view(), name="Plano-detail"),
#     path("create/", PlanoCreate.as_view(), name="Plano-create"),
#     path("update/<int:pk>/", PlanoUpdate.as_view(), name="Plano-update"),
#     path("delete/<int:pk>/", PlanoDelete.as_view(), name="Plano-delete"),

# ]
