# Importações do Django
from django.db import models
from django.utils.translation import gettext_lazy as _


# Modelo do Plano
class Plano(models.Model):

    # Dados do Plano
    nome_plano = models.CharField (_("Nome do Plano"), max_length=255)
    preco_plano = models.DecimalField(_("Preço do Plano"), max_digits=5, decimal_places=2)
    duracao_plano = models.IntegerField(_("Duração do Plano"))
    banda_larga_plano = models.IntegerField(_("Banda Larga do Plano"))

    # Retorno padrão do objeto da classe
    def __str__(self):
        return self.nome_plano
    
    # Configurando o nome da tabela no banco de dados e o nome do aplicativo
    class Meta: 
        db_table = "plano"
        app_label = "plano"

