# Importações do Django
from django.contrib import admin
from django.urls import path, include


urlpatterns = [

    # URLs do Admin
    path('admin/', admin.site.urls),

    # URLs do Sistema
    path("usuario/", include("usuario.urls")),

]
